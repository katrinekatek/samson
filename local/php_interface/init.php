<?php

define('CATALOG_IBLOCK_ID', 2);

function debugConsole($data) {
    global $APPLICATION;
    $APPLICATION->AddHeadString('<script>console.log(' . json_encode($data) . ')</script>');
};
