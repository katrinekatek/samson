<?php

namespace Sprint\Migration;


class Version20210612214647 extends Version
{
    protected $description = 'task #1. Создание формы для ошибок на сайте';

    protected $moduleVersion = '3.28.7';

    const NAME_FORM = 'ERROR_INFORMATION_FORM';
    const FORM_PROPERTIES = [
        'NAME' => 'Информация об ошибках на сайте',
        'SID' => self::NAME_FORM,
        'BUTTON' => 'Спасибо',
        'arSITE' => ['s1'],
        'arMENU' => [
            'ru' => 'Информация об ошибках на сайте - результаты',
            'en' => 'Information about errors on the site - results'
        ]
    ];
    const FIELDS_PROPERTIES = [
        [
            'ACTIVE' => 'Y',
            'TITLE' => 'Строка с ошибкой',
            'ADDITIONAL' => 'N',
            'FIELD_TYPE' => 'text',
            'IN_RESULTS_TABLE' => 'Y',
            'IN_EXCEL_TABLE' => 'Y',
            'FILTER_TITLE' => 'Строка с ошибкой',
            'RESULTS_TABLE_TITLE' => 'Строка с ошибкой',
            'arFILTER_FIELD' => ['text'],
            'arFILTER_ANSWER_TEXT' => ['text'],
            'SID' => 'VS_TEXT',
            'REQUIRED' => 'Y',
            'C_SORT' => 100,
            'arANSWER' => [
                [
                    'ACTIVE' => 'Y',
                    'FIELD_WIDTH' => 40,
                    'FIELD_HEIGHT' => 4,
                    'FIELD_TYPE' => 'textarea',
                    'MESSAGE' => " ",
                ]
            ]
        ],
        [
            'ACTIVE' => 'Y',
            'TITLE' => 'Информация о ошибке',
            'SID' => 'VS_ERROR',
            'ADDITIONAL' => 'N',
            'FIELD_TYPE' => 'text',
            'IN_RESULTS_TABLE' => 'Y',
            'IN_EXCEL_TABLE' => 'Y',
            'FILTER_TITLE' => 'Строка с ошибкой',
            'RESULTS_TABLE_TITLE' => 'Строка с ошибкой',
            'arFILTER_FIELD' => ['text'],
            'arFILTER_ANSWER_TEXT' => ['text'],
            'REQUIRED' => 'Y',
            'C_SORT' => 100,
            'arANSWER' => [
                [
                    'ACTIVE' => 'Y',
                    'FIELD_WIDTH' => 40,
                    'FIELD_HEIGHT' => 4,
                    'FIELD_TYPE' => 'textarea',
                    'MESSAGE' => " ",
                ]
            ]

        ],
        [
            'ACTIVE' => 'Y',
            'TITLE' => 'Адрес',
            'SID' => 'VS_URL',
            'ADDITIONAL' => 'N',
            'FIELD_TYPE' => 'hidden',
            'IN_RESULTS_TABLE' => 'Y',
            'IN_EXCEL_TABLE' => 'Y',
            'FILTER_TITLE' => 'Адрес',
            'RESULTS_TABLE_TITLE' => 'Адрес',
            'arFILTER_FIELD' => ['text'],
            'arFILTER_ANSWER_TEXT' => ['text'],
            'REQUIRED' => 'Y',
            'C_SORT' => 100,
            'arANSWER' => [
                [
                    'ACTIVE' => 'Y',
                    'FIELD_WIDTH' => 40,
                    'FIELD_TYPE' => 'hidden',
                    'MESSAGE' => " ",
                ]
            ]

        ],
    ];

    const STATUSES = [
        [
            "C_SORT" => 100,
            "ACTIVE" => "Y",
            "TITLE" => "Рассмотрено",
            "DESCRIPTION" => "Окончательный статус",
            "CSS" => "statusgreen",
            "HANDLER_OUT" => "",
            "HANDLER_IN" => "",
            "DEFAULT_VALUE" => "N",
            "arPERMISSION_VIEW" => [2],
            "arPERMISSION_MOVE" => [],
            "arPERMISSION_EDIT" => [],
            "arPERMISSION_DELETE" => [],
        ],
        [
            "C_SORT" => 100,
            "ACTIVE" => "Y",
            "TITLE" => "Новый",
            "DESCRIPTION" => "Начальный статус",
            "CSS" => "statusred",
            "HANDLER_OUT" => "",
            "HANDLER_IN" => "",
            "DEFAULT_VALUE" => "Y",
            "arPERMISSION_VIEW" => [2],
            "arPERMISSION_MOVE" => [],
            "arPERMISSION_EDIT" => [],
            "arPERMISSION_DELETE" => [],
        ],
    ];

    public function up()
    {
        \CModule::IncludeModule('form');
        $ID = \CForm::Set(self::FORM_PROPERTIES, false, 'N');
        if ($ID) {
            $this->outSuccess('Форма ' . self::FORM_PROPERTIES['NAME'] . ' создана.');
            foreach (self::FIELDS_PROPERTIES as $field) {
                $field['FORM_ID'] = $ID;
                if (\CFormField::Set($field)) {
                    $this->outSuccess('Поле ' . $field['TITLE'] . ' создано.');
                } else {
                    $this->outError('Ошибка при создании поля ' . $field['TITLE']);
                }
            }
            foreach (self::STATUSES as $status) {
                $status['FORM_ID'] = $ID;
                if (\CFormStatus::Set($status)) {
                    $this->outSuccess('Статус ' . $status['TITLE'] . ' создан.');
                } else {
                    $this->outError('Ошибка при создании статуса ' . $status['TITLE']);
                }
            }
        } else {
            $this->outError('Ошибка при создании формы ' . self::FORM_PROPERTIES['NAME']);
        }
    }

    public function down()
    {
        \CModule::IncludeModule('form');

        $rsForms = \CForm::GetList($by = 's_id', $order = 'desc', self::FORM_PROPERTIES, $is_filtered);
        while ($arForm = $rsForms->Fetch()) {
            $ID = $arForm['ID'];
        }
        if (!empty($ID)) {
            if (\CForm::Delete($ID)) {
                $this->outSuccess('Форма ' . self::FORM_PROPERTIES['NAME'] . ' удалена.');
            } else {
                $this->outError('Ошибка при удалении формы ' . self::FORM_PROPERTIES['NAME']);
            }
        } else {
            $this->outError('Ошибка при удалении формы ' . self::FORM_PROPERTIES['NAME']);
        }
    }
}
