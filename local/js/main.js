$(document).ready(function () {
    BX.bind(document, "keypress", ErrorForm);
    BX.bind(document.querySelector('[name="ERROR_INFORMATION_FORM"]'), "submit", function () {
        var errorSelector = '[name="' + $('#VS_TEXT').text() + '"]';
        $(errorSelector).prop("disabled", false);
        $(this).addClass('busy');
    });

    function ErrorForm(event, formElem) {
        event = event || window.event;
        if ((event.ctrlKey) && ((event.keyCode == 0xA) || (event.keyCode == 0xD)) && getSelected().toString() != "") {
            var dialog = document.querySelector('dialog');
            var urlSelector = '[name="' + $('#VS_URL').text() + '"]';
            $(urlSelector).val(window.location.href);
            var errorSelector = '[name="' + $('#VS_TEXT').text() + '"]';
            $(errorSelector).val(getSelected().toString());
            $(errorSelector).prop("disabled", true);
            dialog.show();
        }
    }

    $('.product-compare').on('click', function () {
        $.ajax({
            url: '/catalog/compare/index.php',
            data: {'action': 'ADD_TO_COMPARE_LIST', 'id': $('.product-compare').data('id')},
            method: 'GET',
            success: function (response) {
                if (response.STATUS === 'OK') {
                    $('.product-compare').attr('href', '/catalog/compare/');
                    $('.product-compare').removeClass('btn-info');
                    $('.product-compare').addClass('btn-primary');
                    $('.product-compare').text('Уже в сравнении');
                }
            }
        })
    });

    $.ajax({
        url: '/local/ajax/basketList.php',
        method: 'POST',
        dataType: 'json',
        data: {'action': 'get_basket'},
        success: function (response) {
            window.basketItems = response.items;
            updateForm();
        }
    });
});

function closew() {
    var dialog = document.querySelector('dialog');
    dialog.close();
}

var getSelected = function () {
    var text = '';
    if (window.getSelection) {
        text = window.getSelection();
    } else if (document.getSelection) {
        text = document.getSelection();
    } else if (document.selection) {
        text = document.selection.createRange().text;
    }
    return text;
};

function updateProductBlock() {
    var productIds = [];
    $.each($('.input_text'), function(key, item) {
        productIds.push($(item).val());
    });

    $.ajax({
        url: '/local/ajax/basketList.php',
        method: 'POST',
        dataType: 'json',
        data: {'action': 'get_product', 'product_ids': productIds},
        success: function (response) {
            window.basketItems = response.items;
            updateForm();
        }
    });
}

function getProductBlock(item, key) {
    if (typeof item['XML_ID'] === 'undefined') {
        item['XML_ID'] = '';
    }
    
    if (typeof item['NAME'] === 'undefined') {
        item['NAME'] = '';
    }
    
    if (typeof item['DETAIL_PICTURE'] === 'undefined') {
        item['DETAIL_PICTURE'] = '';
    }
    
    if (typeof item['DETAIL_TEXT'] === 'undefined') {
        item['DETAIL_TEXT'] = '';
    }
    
    var div = document.createElement('div');
    div.setAttribute('id', 'block_' + key);
    div.setAttribute('data-block-id', key);
    div.classList.add('block');
    var input_text = document.createElement('input');
    input_text.classList.add('input_text');
    input_text.setAttribute('type', 'text');
    input_text.setAttribute('value', item['XML_ID']);
    var input_button_del = document.createElement('input');
    input_button_del.classList.add('input_button_del');
    input_button_del.setAttribute('type', 'button');
    input_button_del.setAttribute('value', 'Удалить');
    var div_product = document.createElement('div');
    div_product.classList.add('block_product');
    var div_name = document.createElement('div');
    div_name.classList.add('product-name');
    div_name.innerText = item['NAME'];
    var product_img = document.createElement('img');
    product_img.setAttribute('src', item['DETAIL_PICTURE']);
    var div_text = document.createElement('div');
    div_text.classList.add('product-text');
    div_text.innerHTML = item['DETAIL_TEXT'];

    div_product.append(div_name);
    div_product.append(product_img);
    div_product.append(div_text);

    div.append(input_text);
    div.append(input_button_del);
    div.append(div_product);

    return div;
}

function updateForm() {
    var key;
    var num = 1;
    var divItems = document.createElement('div');
    for (key in window.basketItems) {
        if (window.basketItems.hasOwnProperty(key)) {
            divItems.append(getProductBlock(window.basketItems[key], key));
            num += 1;
        }
    }

    var form = document.querySelector('.add_basket');
    if (form != null) {
        form.innerHTML = divItems.outerHTML;

        var add_input_button = document.createElement('input');
        add_input_button.setAttribute('type', 'button');
        add_input_button.setAttribute('id', 'add_input');
        add_input_button.setAttribute('value', '+');
        add_input_button.setAttribute('style', 'margin-top: 15px;');
        form.append(add_input_button);

        $('#add_input').on('click', function () {
            window.basketItems['new-' + Date.now()] = {'DETAIL_PICTURE':'', 'DETAIL_TEXT': '', 'ID': '', 'NAME': '', 'XML_ID':''};
            updateForm();
        });

        $('.input_button_del').on('click', function () {
            var key = $(this).closest('.block').data('block-id');
            $(this).closest('.block').remove();
            delete window.basketItems[key];

            updateProductBlock()
        });
    }

    $('input.input_text').on('blur', updateProductBlock);
}
