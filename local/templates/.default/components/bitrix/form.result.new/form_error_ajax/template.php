<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<dialog class="popup_error">
    <?php
    if ($arResult["isFormErrors"] == "Y"):?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
        <?= $arResult["FORM_HEADER"] ?>
        <table>
            <span id="close" class="button_close" onclick="closew()"></span>
            <?
            if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y") {
                ?>
                <tr>
                    <td><?
                        if ($arResult["isFormTitle"]) {
                            ?>
                            <h3><?= $arResult["FORM_TITLE"] ?></h3>
                            <?
                        } //endif ;

                        if ($arResult["isFormImage"] == "Y") {
                            ?>
                            <a href="<?= $arResult["FORM_IMAGE"]["URL"] ?>" target="_blank"
                               alt="<?= GetMessage("FORM_ENLARGE") ?>"><img src="<?= $arResult["FORM_IMAGE"]["URL"] ?>"
                                                                            <? if ($arResult["FORM_IMAGE"]["WIDTH"] > 300): ?>width="300"
                                                                            <? elseif ($arResult["FORM_IMAGE"]["HEIGHT"] > 200): ?>height="200"<? else:?><?= $arResult["FORM_IMAGE"]["ATTR"] ?><? endif;
                                ?> hspace="3" vscape="3" border="0"/></a>
                            <? //=$arResult["FORM_IMAGE"]["HTML_CODE"]
                            ?>
                            <?
                        } //endif
                        ?>

                        <p><?= $arResult["FORM_DESCRIPTION"] ?></p>
                    </td>
                </tr>
                <?
            } // endif
            ?>
        </table>
        <br/>
        <table class="form-table data-table">
            <thead>
            <tr>
                <th colspan="2">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?
            foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                ?>
                <span id="<?= $FIELD_SID ?>" hidden>form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?></span>
                <?
                if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
                    echo $arQuestion["HTML_CODE"];
                } else {
                    ?>
                    <tr>
                        <td>
                            <?
                            if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
                                <span class="error-fld"
                                      title="<?= htmlspecialcharsbx($arResult["FORM_ERRORS"][$FIELD_SID]) ?>"></span>
                            <?endif; ?>
                            <?= $arQuestion["CAPTION"] ?><?
                            if ($arQuestion["REQUIRED"] == "Y"):?><?= $arResult["REQUIRED_SIGN"]; ?><?endif; ?>
                            <?= $arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />" . $arQuestion["IMAGE"]["HTML_CODE"] : "" ?>
                        </td>
                        <td><?= $arQuestion["HTML_CODE"] ?></td>
                    </tr>
                    <?
                }
            } //endwhile
            ?>
            <?
            ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="2">
                    <? if ($arResult["F_RIGHT"] >= 15):?>
                        &nbsp;<input type="hidden" name="web_form_apply" value="Y"/><input type="submit"
                                                                                           name="web_form_apply"
                                                                                           value="<?= GetMessage("FORM_APPLY") ?>"/>
                    <? endif; ?>
                </th>
            </tr>
            </tfoot>
        </table>
        <p>
            <?= $arResult["REQUIRED_SIGN"]; ?> - <?= GetMessage("FORM_REQUIRED_FIELDS") ?>
        </p>
        <?= $arResult["FORM_FOOTER"] ?>
        
</dialog>
