<?php

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');

$action = $_POST['action'];
$result = [
    'items' => [],
];

$getProducts = function ($productIds, $field) {
    $result = [];
    if (empty($productIds)) {
        return $result;
    }

    if (!is_array($productIds)) {
        $productIds = (array)$productIds;
    }

    $dbRes = CIBlockElement::GetList(['ID' => 'ASC'], [$field => $productIds], false, false, ['ID', 'XML_ID', 'PROPERTY_ARTNUMBER', 'PROPERTY_COLOR_REF', "PROPERTY_SIZES_CLOTHES", 'NAME', 'DETAIL_PICTURE']);
    while ($fields = $dbRes->Fetch()) {
        $renderImage = CFile::ResizeImageGet($fields['DETAIL_PICTURE'], ['width' => 150, 'height' => 150]);

        $result[$fields['ID']] = [
            'ID' => $fields['ID'],
            'XML_ID' => $fields['XML_ID'],
            'NAME' => $fields['NAME'],
            'DETAIL_TEXT' => 'Цвет : ' . $fields['PROPERTY_COLOR_REF_VALUE'] . '<br>' . 'Размер : ' . $fields['PROPERTY_SIZES_CLOTHES_VALUE'] . '<br>' . 'Артикул : ' . $fields['PROPERTY_ARTNUMBER_VALUE'] . '<br>',
            'DETAIL_PICTURE' => $renderImage['src'],
        ];
    }

    foreach ($productIds as $productId) {
        if ($productId == '') {
            $result['new-' . microtime()] = [
                'ID' => '',
                'XML_ID' => '',
                'NAME' => '',
                'DETAIL_TEXT' => '',
                'DETAIL_PICTURE' => '',
            ];
        }
    }

    return $result;
};

$getBasket = function () {
    $result = [];
    $dbRes = CSaleBasket::GetList(
        ['ID' => 'ASC'],
        [
            "FUSER_ID" => \Bitrix\Sale\Fuser::getId(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL",
        ],
        false,
        false,
        ['ID', 'IBLOCK_ID', 'PRODUCT_ID']
    );
    while ($fields = $dbRes->Fetch()) {
        $result[$fields['PRODUCT_ID']] = [
            'ID' => $fields['PRODUCT_ID'],
            'ID_BASKET' => $fields['ID']
        ];
    }

    return $result;
};

switch ($action):
    case 'get_basket':
    {
        $result['items'] = $getBasket();
        $productIds = array_keys($result['items']);
        $result['items'] = $getProducts($productIds, 'ID');

        break;
    }
    case 'get_product':
    {
        if (!empty($_POST['product_ids'])) {
            if (!is_array($_POST['product_ids'])) {
                $_POST['product_ids'] = (array)$_POST['product_ids'];
            }

            $productIds = [];
            foreach ($_POST['product_ids'] as $productId) {
                $productIds[] = $productId !== '' ? (int)$productId : '';
            }


            $result['items'] = $getProducts($productIds, 'XML_ID');
            $basket = \Bitrix\Sale\Basket::LoadItemsForFUser(
                \Bitrix\Sale\Fuser::getId(),
                SITE_ID
            );
            $basketItems = $getBasket();

            foreach ($result['items'] as $key => $item) {
                if (!in_array($key, $basketItems)) {
                    Add2BasketByProductID(
                        1,
                        []
                    );
                }
            }

            foreach ($basketItems as $key => $item) {
                if (!key_exists($key, $result['items'])) {
                    \CSaleBasket::Delete($item['ID_BASKET']);
                }
            }
        }
    }
endswitch;

echo json_encode($result);
